<?php

namespace App\Http\Controllers\Auth;

use App\Http\Requests\RegisterRequest;
use App\User;
use Validator;
use Auth;
use View;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }

    public function  login_form()
    {
        View::share('razdel', 'auth');
        $error = 0;
        return view('auth.auth', compact('error'));
    }

    public function  register_success()
    {
        View::share('razdel', 'login');
        return view('auth.reg_succses');
    }

    public function  sign_up_form()
    {
        View::share('razdel', 'login');
        return view('auth.login');
    }

    public function authenticate(Request $request)
    {
        $userdata = array(
            'login' => $request->get('login'),
            'password' => $request->get('password'),
        );
        $remember_me = $request->get('remember-me');
        $validator = Validator::make($request->all(), [
            'login' => 'required|max:100',
            'password' => 'required|min:6',
        ]);
        if ($remember_me) {
            if (Auth::attempt($userdata,true)) {
                return redirect('/');

            } else {
                return redirect('auth/login')
                    ->withErrors($validator);
            }
        } else {
            if (Auth::attempt($userdata)) {
                return redirect('/');

            } else {
                return redirect('auth/login')
                    ->withErrors($validator);
            }
        }
    }

    public function register(RegisterRequest $request)
    {
        $validator = Validator::make($request->all(), [
            'login' => 'required|unique:users|max:100',
            'password' => 'required|confirmed|min:6',
        ]);
        if ($validator->fails()) {
            return redirect('/auth/register')
                ->withErrors($validator)
                ->withInput();
        } else {
            $user = User::create([
                'login' => $request->input('login'),
                'password' => bcrypt($request->input('password')),
            ]);
            $user->save();
            return redirect('/auth/register_success');
        }
    }

    public function logout()
    {
        Auth::logout();
        return redirect('/auth/login');
    }

}
