@extends('layout')
@section('title')
    @if (\Auth::check())
        <ul class="nav pull-right">
            <li><a>{{\Auth::user()->login}}</a></li>
            <li><a href="{{ URL::to('logout') }}">Выход</a></li>
        </ul>
    @else
@section('notauth')
    <li @if ($razdel==='auth') class='active' @endif><a href="/auth/login">Авторизация</a></li>
    <li @if ($razdel==='login') class='active' @endif><a href="/auth/register">Регистрация</a></
@endsection
@endif
@endsection
@section('content')
    <div class="row-fluid">
        <div class="span2"></div>
        <div class="span8">
            {!!  Form::open(['url'=>'/']) !!}
            <form action="" method="post" class="form-horizontal" style="margin-bottom: 50px;">
                @if (count($errors) > 0)
                    <div class="alert alert-error">
                        Сообщение не может быть пустым
                    </div>
                @endif
                @if (\Auth::check())
                    <div class="control-group">
                        {!!Form::textarea('textarea',null, array('style'=>'width: 100%;height: 50px;','type'=>"password",'placeholder' => 'Ваше сообщение...'))!!}
                    </div>
                    <div class="control-group">
                        {!!Form::submit('Отправить сообщение', array('class' => 'btn btn-primary'))!!}
                    </div>
                @endif
            </form>
            {!!  Form::close()!!}

            @foreach($messages as $message)
                <div class="well">
                    <h5>{{$message->User->login}}:</h5>
                    {{$message->text}}
                </div>
            @endforeach
        </div>
    </div>
@endsection