@extends('layout')
@section('notauth')
    <li @if ($razdel==='auth') class='active' @endif><a href="/auth/login">Авторизация</a></li>
    <li @if ($razdel==='login') class='active' @endif><a href="/auth/register">Регистрация</a></
@endsection
@section('content')
    <div class="row-fluid">
        <div class="span4"></div>
        <div class="span8">
            {!!  Form::open(array('class' => 'form-horizontal','method'=>'post','url'=>'/auth/register')) !!}
            <div class="control-group">
                <b>Регистрация</b>
            </div>

            @if (count($errors) > 0)
                <div class="control-group error">
                    {!! Form::text('login',null, array('id' => 'inputLogin', 'placeholder' => 'Логин','autocomplete'=>'off')) !!}
                    <span class="help-inline">Регистрация с указанными данными невозможна</span>
                </div>
                <div class="control-group error">
                    {!! Form::password('password', array('id' => 'inputPassword2', 'placeholder' => 'Пароль')) !!}
                </div>

                <div class="control-group error">
                    {!! Form::password('password_confirmation', array('id' => 'inputPassword2', 'placeholder' => 'Пароль')) !!}
                </div>
            @else
                <div class="control-group">
                    {!! Form::text('login',null, array('id' => 'inputLogin', 'placeholder' => 'Логин','autocomplete'=>'off')) !!}
                </div>
                <div class="control-group">
                    {!! Form::password('password', array('id' => 'inputPassword', 'placeholder' => 'Пароль')) !!}
                </div>
                <div class="control-group">
                    {!! Form::password('password_confirmation', array('id' => 'inputPassword2', 'placeholder' => 'Пароль')) !!}
                </div>

                @endif
                <div class="control-group">
                    {!!Form::submit('Отправить', array('class' => 'btn btn-primary'))!!}
                </div>
                {!! Form::close() !!}
        </div>
    </div>
@endsection