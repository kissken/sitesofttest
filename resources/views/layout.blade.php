<!DOCTYPE html>
<html>
<head>
    <title>Сайтсофт</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
    <script src="http://code.jquery.com/jquery.js"></script>
    <script src="/bootstrap/js/bootstrap.min.js"></script>
</head>
<body>

<div class="navbar">
    <div class="navbar-inner">
        <a class="brand" href="/">Сайтсофт</a>
        <ul class="nav">
            <li @if ($razdel==='index') class='active' @endif><a href="/">Главная</a></li>
            @yield('notauth')
        </ul>
        @yield('title')
    </div>
</div>
@yield('content')


</body>
</html>