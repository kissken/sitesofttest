@extends('layout')
@section('notauth')
    <li @if ($razdel==='auth') class='active' @endif><a href="/auth/login">Авторизация</a></li>
    <li @if ($razdel==='login') class='active' @endif><a href="/auth/register">Регистрация</a></li>
@endsection
@section('content')
    <div class="row-fluid">
        <div class="span4"></div>
        <div class="span3">

            {!!  Form::open(array('class' => 'form-horizontal','method'=>'post','url'=>'/auth/login')) !!}
                {!! csrf_field() !!}
            @if (count($errors) > 0)
                <div class="alert alert-error">
                    Вход в систему с указанными данными невозможен
                </div>
            @endif
                <div class="control-group">
                    <b>Авторизация</b>
                </div>
                <div class="control-group">
                    {!! Form::text('login',null, array('id' => 'inputLogin', 'placeholder' => 'Логин','autocomplete'=>'off')) !!}
                </div>
                <div class="control-group">
                    {!! Form::password('password', array('id' => 'inputPassword', 'placeholder' => 'Пароль')) !!}
                </div>
                <div class="control-group">
                    <label class="checkbox">
                        {!! Form::checkbox('remember-me', 1)!!}Запомнить меня
                    </label>
                    {!!Form::submit('Вход', array('class' => 'btn btn-primary'))!!}
                </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection