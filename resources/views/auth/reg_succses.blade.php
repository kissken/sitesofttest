@extends('layout')
@section('notauth')
    <li @if ($razdel==='auth') class='active' @endif><a href="/auth/login">Авторизация</a></li>
    <li @if ($razdel==='login') class='active' @endif><a href="/auth/register">Регистрация</a></li>
@endsection
@section('content')
    <div class="row-fluid">
        <div class="span2"></div>
        <div class="span8">

            <h3>Ура!</h3>

            <p>
                Поздравляем! Вы успешно зарегистрировались.
            </p>

            <p>
                Воспользуйтесь <a href="/auth/login">формой авторизации</a> чтобы войти на сайт под своей учетной записью
            </p>
        </div>
    </div>
@endsection