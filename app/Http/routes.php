<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'PageController@index');
Route::post('/', 'PageController@post');

Route::get('/auth/login', 'Auth\AuthController@login_form');
Route::post('/auth/login', 'Auth\AuthController@authenticate');
Route::get('/logout', 'Auth\AuthController@logout');


Route::get('/auth/register', 'Auth\AuthController@sign_up_form');
Route::post('/auth/register', 'Auth\AuthController@register');
Route::get('/auth/register_success', 'Auth\AuthController@register_success');



